import React from 'react';
import './style.css';

export default function Brand() {
  return (
    <i className="brand">
      Pixalione<i>Learn</i>
    </i>
  );
}
