
# Pixalion Learn Platform

Pixalion Learn Platform is an E-learning Platform App in the three-tier mode,
the UI is made with react which communicates via authorized requests with a node 
server communicating with a cloud database cluster.



## Tech Stack

**Client:** React

**Server:** Node, Express

**Database** MongoDB Atlas



## Demo

you can watch a live demo of the application in this loom screen record link 
https://www.loom.com/share/e820a0f217084fb5ad8619a551b50f74

## Run Locally

Make sure you have exactly the version 14.18.0 of NODE to avoid any dependencies conflict while installing packages 
here is a link for Node v14.18.0 https://www.npackd.org/p/org.nodejs.NodeJS/14.18

pull the server :

```bash
  git init 
  git remote add origin https://github.com/TalelKarim/pixalion_test_server.git
  git pull origin master
```

Go to the project directory

```bash
  cd my-project
```

Install dependencies

```bash
  npm install
```

Start the server

```bash
  npm start
```


pull the Client : 
```bash
  git init 
  git remote add origin https://github.com/TalelKarim/pixalion_test_front.git
  git pull origin master
```

Go to the project directory

```bash
  cd my-project
```

Install dependencies

```bash
  npm install
```

Start the server

```bash
  npm start
```




## Made By

This project is made By :
https://www.linkedin.com/in/talel-karim-chebbi-806920233/

